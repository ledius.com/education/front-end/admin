import { PurchaseCourseItem } from "@/domain/response/purchase/course/PurchaseCourseItem";

export class PurchaseCourse {
  public readonly id: string;
  public readonly userId: string;
  public readonly courseId: string;
  public readonly isPaid: boolean;

  constructor (id: string, userId: string, courseId: string, isPaid: boolean) {
    this.id = id;
    this.userId = userId;
    this.courseId = courseId;
    this.isPaid = isPaid;
  }

  public static of (purchaseItem: PurchaseCourseItem): PurchaseCourse {
    return new PurchaseCourse(
      purchaseItem.id,
      purchaseItem.userId,
      purchaseItem.courseId,
      purchaseItem.isPaid
    );
  }
}
