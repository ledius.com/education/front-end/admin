export interface PaymentResponse {
  readonly paymentUrl: string;
}
