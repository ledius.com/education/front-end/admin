export interface BlogPostResponse {
  url: string;
  name: string;
  content: string;
  image: string;
}
