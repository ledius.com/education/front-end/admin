export interface AuthConfirmResponse {
  accessToken: string;
  refreshToken: string;
  type: string;
  expiresIn: string;
}
