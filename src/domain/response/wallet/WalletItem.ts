export interface WalletItem {
  id: string;
  ownerId: string;
  amount: number;
}
