export interface CourseItem {
  id: string;
  content: {
    icon: string;
    name: string;
  };
  price: number;
  isPublished: boolean;
}
