export interface ProfileResponse {
  readonly id: string;
  readonly userId: string;
  readonly firstName: string;
  readonly lastName: string;
}
