export class EmailAuthRequestForm {
  public readonly email: string;

  constructor (email: string) {
    this.email = email;
  }
}
