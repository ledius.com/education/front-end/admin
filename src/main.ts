import 'reflect-metadata';
import Vue from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import i18n from './i18n';
import './plugins/axios';
import './plugins/mask';
import filters from '@/filters';
import Container from '@/components/common/Container.vue';

Vue.use(filters);
Vue.component('container', Container);

Vue.config.productionTip = false;
(async () => {
  await store.dispatch('auth/check');

  new Vue({
    router,
    store,
    vuetify,
    i18n,
    render: h => h(App)
  }).$mount('#app');
})();
