import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import store from '@/store';
import { DialogError } from '@/store/module/errors';
import { instanceToPlain } from 'class-transformer';
import { TokenResponse } from '@ledius/software-development-kit';
import { isAfter } from 'date-fns';

axios.interceptors.response.use(undefined, async error => {
  const response: AxiosResponse = error.response;
  if (!response) {
    throw error;
  }
  if (response.status === 409) {
    await store.commit('errors/push', new DialogError(response.data.message));
  }
  if (response.status === 401) {
    await store.dispatch('auth/logout');
  }

  throw error;
});

async function setLanguage(
  config: AxiosRequestConfig
): Promise<AxiosRequestConfig> {
  config.headers = {
    ...(config.headers || {}),
    'Accept-Language': 'ru'
  };
  return config;
}

async function setAuthorizationToken(config: AxiosRequestConfig) {
  const token: TokenResponse | null = await store.getters['auth/token'];
  if (!token) {
    return config;
  }
  config.headers.Authorization = `${token.type} ${token.accessToken}`;
  console.log('set authorization token');
  return config;
}

async function refreshAuthorizationTokenIfExpired(config: AxiosRequestConfig) {
  const isExpired = (token: TokenResponse): boolean =>
    isAfter(new Date(), new Date(token.expiresIn));
  const token: TokenResponse | null = await store.getters['auth/token'];
  if (!token) {
    return config;
  }
  if (isExpired(token) && config.url !== '/api/v1/auth/refresh') {
    await store.dispatch('auth/refresh');
  }
  console.log(
    isExpired(token),
    new Date(token.expiresIn),
    new Date(),
    config.url
  );
  return config;
}

async function classToPlainInterceptor(config: AxiosRequestConfig) {
  if (typeof config.data === 'object') {
    config.data = instanceToPlain(config.data);
  }
  if (typeof config.params === 'object') {
    config.params = instanceToPlain(config.params);
  }
  return config;
}

axios.interceptors.request.use(async config => {
  return classToPlainInterceptor(
    await setLanguage(
      await setAuthorizationToken(
        await refreshAuthorizationTokenIfExpired(config)
      )
    )
  );
});
