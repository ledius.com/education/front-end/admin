import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import { routeAuthGuard } from '@/router/hook/routeAuthGuard';
import { routeGuestGuard } from '@/router/hook/routeGuestGuard';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/exchange/OffersView.vue'),
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/signup',
    name: 'Signup',
    component: () =>
      import(/* webpackChinkName: "signup" */ '../views/signup/SignupView.vue'),
    meta: {
      requiredGuest: true
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/login/LoginView.vue'),
    meta: {
      requiredGuest: true
    }
  },
  {
    path: '/courses',
    name: 'Courses',
    component: () => import('../views/course/CourseView.vue'),
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/lessons',
    name: 'Lessons',
    component: () => import('../views/lesson/LessonView.vue'),
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/lesson/:id',
    name: 'Lesson.Content',
    component: () => import('../views/lesson/content/LessonContentView.vue'),
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/consultations',
    name: 'Consultations',
    component: () => import('../views/consultations/ConsultationsView.vue'),
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/profile',
    name: 'Profile',
    component: () => import('../views/profile/ProfileView.vue'),
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/bids',
    name: 'Bids',
    component: () => import('../views/bid/BidsView.vue'),
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/bids/new',
    name: 'Bids.New',
    component: () => import('../views/bid/BidCreateView.vue'),
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/blog/:path',
    name: 'Post',
    component: () => import('../views/blog/PostView.vue'),
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/auth/:refreshToken',
    name: 'Auth',
    component: () => import('../views/auth/AuthRefreshView.vue'),
    meta: {
      requiredAuth: false
    }
  },
  {
    path: '/exchange',
    name: 'Exchange',
    component: () => import('../views/exchange/ExchangeView.vue'),
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/bank-details',
    name: 'BankDetails',
    component: () => import('../views/exchange/BankDetailsView.vue'),
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/offers',
    name: 'Offers',
    component: () => import('../views/exchange/OffersView.vue'),
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/offers/new',
    name: 'OffersCreate',
    component: () => import('../views/exchange/OfferCreateView.vue'),
    meta: {
      requiredAuth: true
    }
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach(routeAuthGuard);
router.beforeEach(routeGuestGuard);

export default router;
