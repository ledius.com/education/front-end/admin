import { VueConstructor } from 'vue';
import { fromScaledAmount } from '@/utils';

export default {
  install(Vue: VueConstructor): void {
    Vue.filter('fromScaledAmount', (value: string, decimals?: number) => {
      return fromScaledAmount(value, decimals);
    });

    Vue.filter('fromScaledPrice', (value: string|number): number => {
      return Number(value) / 100;
    });

    Vue.filter('localDateString', (value: string|Date) => {
      return new Date(value).toLocaleString();
    });
  }
};
