import { Consultation } from "@/domain/entity/consultation/Consultation";
import { Module } from "vuex";
import axios from "axios";
import { ConsultationResponse } from "@/domain/response/consultations/ConsultationResponse";

type State = {
  consultations: Consultation[];
};

export default {
  namespaced: true,
  state: {
    consultations: []
  },
  getters: {
    items: (state) => state.consultations
  },
  mutations: {
    set: (state, payload: Consultation[]): void => {
      state.consultations = payload;
    }
  },
  actions: {
    fetch: async ({ commit }): Promise<void> => {
      const { data } = await axios.get<ConsultationResponse[]>("/api/v1/consultations");
      const consultations = data.map(Consultation.from);
      commit('set', consultations);
    }
  }
} as Module<State, unknown>;
