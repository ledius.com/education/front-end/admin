import { Module } from "vuex";

export class DialogError {
  public readonly message: string;

  constructor (message: string) {
    this.message = message;
  }
}
type State = {
  errors: DialogError[]
};

export default {
  namespaced: true,
  state: {
    errors: []
  },
  getters: {
    has: (state: State): boolean => state.errors.length > 0,
    items: (state: State): DialogError[] => state.errors
  },
  mutations: {
    push: (state: State, error: DialogError): void => {
      state.errors = [
        ...state.errors,
        error
      ];
    },
    remove: (state: State, error: DialogError): void => {
      const e = state.errors.indexOf(error);
      state.errors = state.errors.filter((_, i) => i !== e);
    },
    clear: (state: State): void => {
      state.errors = [];
    }
  }
} as Module<State, unknown>;
