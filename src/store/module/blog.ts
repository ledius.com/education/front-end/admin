import { BlogPostResponse } from '@/domain/response/blog/BlogPostResponse';
import { Module } from 'vuex';
import { RootState } from '@/store';
import axios from 'axios';

type State = {
  posts: BlogPostResponse[];
};

export default {
  namespaced: true,
  state: {
    posts: []
  },
  getters: {
    items: state => state.posts,
    loaded: state => state.posts.length !== 0,
    getByUrl: state => (url: string) =>
      state.posts.find(post => post.url === url)
  },
  mutations: {
    set: (state, items) => (state.posts = items)
  },
  actions: {
    fetch: async ({ commit, getters }): Promise<void> => {
      if (getters.loaded) {
        return;
      }
      const { data } = await axios.get<BlogPostResponse[]>('/api/v1/blog');
      commit('set', data);
    }
  }
} as Module<State, RootState>;
