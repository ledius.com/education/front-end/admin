import { Module } from 'vuex';
import { RootState } from '@/store';
import { Wallet } from '@/domain/entity/wallet/Wallet';
import axios from 'axios';
import { WalletResponse } from '@/domain/response/wallet/WalletResponse';

type State = {
  wallet: Wallet | null;
};

export default {
  namespaced: true,
  state: {
    wallet: null
  },
  getters: {
    item: state => state.wallet
  },
  mutations: {
    set: (state, item: Wallet | null): void => {
      state.wallet = item;
    }
  },
  actions: {
    fetch: async ({ commit }, ownerId: string): Promise<void> => {
      const { data } = await axios.get<WalletResponse>('/api/v1/wallets', {
        params: { ownerId }
      });
      commit('set', Wallet.from(data));
    }
  }
} as Module<State, RootState>;
