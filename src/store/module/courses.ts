import { Module } from "vuex";
import { Course } from "@/domain/entity/course/Course";
import axios from "axios";
import { CourseListResponse } from "@/domain/response/courses/CourseListResponse";
import { CourseItem } from "@/domain/response/courses/CourseItem";

type State = {
  courses: Course[];
  course: Course|null,
};

export default {
  namespaced: true,
  state: {
    courses: [],
    course: null
  },
  getters: {
    items: state => state.courses,
    course: state => state.course
  },
  mutations: {
    set: (state: State, items: Course[]): void => {
      state.courses = items;
    },
    setCourse: (state: State, course: Course): void => {
      state.course = course;
    }
  },
  actions: {
    fetch: async ({ commit }): Promise<void> => {
      const response = await axios.get<CourseListResponse>("/api/v1/courses");
      const courses = response.data.items.map(item =>
        new Course(
          item.id,
          item.content.icon,
          item.content.name,
          item.price)
      );
      commit('set', courses);
    },
    fetchCourse: async ({ commit }, id: string): Promise<void> => {
      const { data } = await axios.get<CourseItem>(`/api/v1/course/${id}`);
      const course = new Course(data.id, data.content.icon, data.content.name, data.price);
      commit('setCourse', course);
    }
  }
} as Module<State, unknown>;
