import { PurchaseCourse } from "@/domain/entity/purchase/course/PurchaseCourse";
import { Module } from "vuex";
import { RootState } from "@/store";
import axios from "axios";
import { PurchasesCourseListResponse } from "@/domain/response/purchase/course/PurchasesCourseListResponse";
import { PurchasePayForm } from "@/domain/form/purchase/PurchasePayForm";

type State = {
  purchases: PurchaseCourse[]
};

export default {
  namespaced: true,
  state: {
    purchases: []
  },
  getters: {
    items: state => state.purchases
  },
  mutations: {
    set: (state: State, payload: PurchaseCourse[]) => {
      state.purchases = payload;
    }
  },
  actions: {
    fetch: async ({ commit }, userId: string): Promise<void> => {
      const { data } = await axios.get<PurchasesCourseListResponse>(`/api/v1/purchases/course?userId=${userId}`);
      const purchases = data.items.map(PurchaseCourse.of);
      commit('set', purchases);
    },
    pay: async ({ commit }, form: PurchasePayForm): Promise<void> => {
      const { data } = await axios.post("/api/v1/purchase/course/pay", form);
      window.open(data.paymentUrl, "_blank");
    }
  }
} as Module<State, RootState>;
