import { Module } from 'vuex';
import {
  CloseOfferCommand,
  ConfirmOfferCommand,
  CreateOfferCommand,
  OfferApi,
  OfferApiListRequest,
  OfferResponse, PaginationResponse,
  PayOfferCommand,
  ReserveOfferCommand
} from '@/api/ledius-token';
import { configuration } from '@/api/configuration';

export type Balance = {
  balance: string;
  symbol: string;
};

type State = {
  sellOffers: OfferResponse[];
  buyOffers: OfferResponse[];
  offers: OfferResponse[];
  pagination: PaginationResponse | null;
};

export const BUY_OFFER_TYPE = 0;
export const SELL_OFFER_TYPE = 1;

const offerApi = new OfferApi(configuration());

export default {
  namespaced: true,
  state: {
    sellOffers: [],
    buyOffers: [],
    offers: [],
    pagination: null
  },
  getters: {
    buyOffers: state => state.buyOffers,
    sellOffers: state => state.sellOffers,
    offers: state => state.offers,
    pagination: state => state.pagination
  },
  mutations: {
    setOffers: (state, payload: OfferResponse[]): void => {
      const { sell, buy } = payload.reduce(
        ({ sell, buy }, offer) => {
          if ((offer.type as never) === BUY_OFFER_TYPE) {
            buy.push(offer);
          }
          if ((offer.type as never) === SELL_OFFER_TYPE) {
            sell.push(offer);
          }
          return { sell, buy };
        },
        { sell: [], buy: [] } as Record<string, OfferResponse[]>
      );
      if (sell.length) {
        state.sellOffers = sell;
      }
      if (buy.length) {
        state.buyOffers = buy;
      }
      state.offers = payload;
    },
    removeOffer: (state, payload: OfferResponse): void => {
      state.buyOffers = state.buyOffers.filter(
        offer => offer.id !== payload.id
      );
      state.sellOffers = state.sellOffers.filter(
        offer => offer.id !== payload.id
      );
    },
    setPagination: (state, pagination: PaginationResponse): void => {
      state.pagination = pagination;
    }
  },
  actions: {
    create: async (_, createOfferCommand: CreateOfferCommand): Promise<void> => {
      await offerApi.create({ createOfferCommand }).then(({ data }) => data);
    },
    confirmPayment: async(_, confirmOfferCommand: ConfirmOfferCommand): Promise<void> => {
      await offerApi.reserveConfirm({ confirmOfferCommand }).then(({ data }) => data);
    },
    close: async(_, closeOfferCommand: CloseOfferCommand): Promise<void> => {
      await offerApi.close({ closeOfferCommand }).then(({ data }) => data);
    },
    fetchOffers: async (
      { commit },
      query: OfferApiListRequest
    ): Promise<void> => {
      const { items, pagination } = await offerApi.list(query).then(({ data }) => data);
      commit('setOffers', items);
      commit('setPagination', pagination);
    },
    pay: async (_, payOfferCommand: PayOfferCommand): Promise<void> => {
      const paymentResult = await offerApi
        .pay({
          payOfferCommand
        })
        .then(({ data }) => data);
      window.open(paymentResult.paymentUrl, '_blank');
    },
    reserve: async (
      { commit },
      reserveOfferCommand: ReserveOfferCommand
    ): Promise<void> => {
      const reservedOffer = await offerApi
        .reserve({ reserveOfferCommand })
        .then(({ data }) => data);
      commit('removeOffer', reservedOffer);
    }
  }
} as Module<State, unknown>;
