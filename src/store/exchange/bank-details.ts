import { Module } from 'vuex';
import {
  BankDetailsApi,
  BankDetailsApiListRequest,
  BankDetailsEnsureExistsCommand,
  BankDetailsResponse
} from '@/api/ledius-token';
import { configuration } from '@/api/configuration';

type State = {
  bankDetails: BankDetailsResponse[];
};

const bankDetailsApi = new BankDetailsApi(configuration());

export default {
  namespaced: true,
  state: {
    bankDetails: []
  },
  getters: {
    bankDetails: state => state.bankDetails
  },
  mutations: {
    ensure: (state, bankDetails: BankDetailsResponse) => {
      state.bankDetails = state.bankDetails
        .filter(details => details.id !== bankDetails.id)
        .concat(bankDetails);
    },
    bankDetails: (state, bankDetails: BankDetailsResponse[]): void => {
      state.bankDetails = bankDetails;
    }
  },
  actions: {
    ensure: async (
      { commit },
      bankDetailsEnsureExistsCommand: BankDetailsEnsureExistsCommand
    ): Promise<void> => {
      const bankDetails = await bankDetailsApi
        .ensure({ bankDetailsEnsureExistsCommand })
        .then(({ data }) => data);
      commit('ensure', bankDetails);
    },
    fetch: async (
      { commit },
      bankDetailsApiListRequest: BankDetailsApiListRequest
    ): Promise<void> => {
      const bankDetails = await bankDetailsApi
        .list(bankDetailsApiListRequest)
        .then(({ data }) => data);
      commit('bankDetails', bankDetails);
    }
  }
} as Module<State, unknown>;
