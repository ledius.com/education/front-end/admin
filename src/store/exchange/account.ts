import { Module } from 'vuex';
import {
  AccountApi,
  AccountCreateCommand,
  AccountResponse
} from '@/api/ledius-token';
import { configuration } from '@/api/configuration';

type State = {
  authAccount: AccountResponse | null;
};

const accountApi = new AccountApi(configuration());

export default {
  namespaced: true,
  state: {
    authAccount: null
  },
  getters: {
    authAccount: state => state.authAccount
  },
  mutations: {
    authAccount: (state, payload: AccountResponse): void => {
      state.authAccount = payload;
    }
  },
  actions: {
    create: async (
      _,
      accountCreateCommand: AccountCreateCommand
    ): Promise<AccountResponse> => {
      return await accountApi
        .create({ accountCreateCommand })
        .then(response => response.data);
    },

    fetchAccount: async (
      { commit },
      userId: string
    ): Promise<AccountResponse> => {
      const account = await accountApi
        .userAccount({
          userId
        })
        .then(response => response.data);
      commit('authAccount', account);
      return account;
    }
  }
} as Module<State, unknown>;
