import Vue from 'vue';
import Vuex from 'vuex';
import auth from '@/store/module/auth';
import errors from '@/store/module/errors';
import courses from '@/store/module/courses';
import lessons from '@/store/module/lessons';
import { lessonContent } from '@/store/module/lesson-content';
import purchasesCourse from '@/store/module/purchasesCourse';
import consultations from '@/store/module/consultations';
import profile from '@/store/module/profile';
import bids from '@/store/module/bids';
import wallet from '@/store/module/wallet';
import blog from '@/store/module/blog';
import account from '@/store/exchange/account';
import balance from '@/store/exchange/balance';
import offers from '@/store/exchange/offers';
import bankDetails from '@/store/exchange/bank-details';

Vue.use(Vuex);

export type RootState = Record<string, never>;

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    auth,
    bids,
    blog,
    errors,
    courses,
    consultations,
    lessons,
    lessonContent,
    purchasesCourse,
    profile,
    wallet,
    exchange: {
      namespaced: true,
      modules: {
        account,
        balance,
        offers,
        bankDetails
      }
    }
  }
});
