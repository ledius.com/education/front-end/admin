export enum OfferTypeEnum {
  BUY = 0,
  SELL = 1
}

export enum OfferStatusEnum {
  OPENED,
  RESERVED,
  FULFILLED,
  CLOSED
}
